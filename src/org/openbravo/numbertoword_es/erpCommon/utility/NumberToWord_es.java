/*
 *  *************************************************************************
 *  The contents of this file are subject to the Creative Commons 
 *  Attribution-ShareAlike 2.5 Spain License 
 *  (the "License"): http://creativecommons.org/licenses/by-sa/2.5/es/. 
 *  Software distributed under the License is  distributed  on  an "AS IS" 
 *  basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *  See the License for the specific  language  governing  rights  
 *  and  limitations under the License.
 *  This is a packaging in a module done by Openbravo SLU of the code snippet
 *  "POS - Number To Words In Ticket (Spanish)" 
 *  by Macumbero and Zil__, published on Openbravo wiki, 
 *  http://wiki.openbravo.com/wiki/POS_-_Number_To_Words_In_Ticket_(Spanish) 
 *  and used under the License.
 *  *************************************************************************
 */

package org.openbravo.numbertoword_es.erpCommon.utility;

import java.math.BigDecimal;

import org.openbravo.numbertoword.erpCommon.utility.NumberToWord;

public class NumberToWord_es extends NumberToWord {
  private static String[] _grupos = { "", "millon", "billon", "trillon" };

  private static String[] _unidades = { "", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete",
      "ocho", "nueve" };

  private static String[] _decena1 = { "", "once", "doce", "trece", "catorce", "quince",
      "dieciseis", "diecisiete", "dieciocho", "diecinueve" };

  private static String[] _decenas = { "", "diez", "veinte", "treinta", "cuarenta", "cincuenta",
      "sesenta", "setenta", "ochenta", "noventa" };

  private static String[] _centenas = { "", "cien", "doscientos", "trescientos", "cuatrocientos",
      "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos" };

  public static String millarATexto(int num) {
    int n = num;

    if (n == 0) {
      return "";
    }

    int centenas = n / 100;
    n = n % 100;
    int decenas = n / 10;
    int unidades = n % 10;

    String sufijo = "";

    if (decenas == 0 && unidades != 0) {
      sufijo = _unidades[unidades];
    }

    if (decenas == 1 && unidades != 0) {
      sufijo = _decena1[unidades];
    }

    if (decenas == 2 && unidades != 0) {
      sufijo = "veinti" + _unidades[unidades];
    }

    if (unidades == 0) {
      sufijo = _decenas[decenas];
    }

    if (decenas > 2 && unidades != 0) {
      sufijo = _decenas[decenas] + " y " + _unidades[unidades];
    }

    if (centenas != 1) {
      return _centenas[centenas] + " " + sufijo;
    }

    if (unidades == 0 && decenas == 0) {
      return "cien";
    }

    return "ciento " + sufijo;
  }

  public static String numeroACastellano(long num) {
    long n = num;

    String resultado = "";
    int grupo = 0;
    while (n != 0 && grupo < _grupos.length) {
      long fragmento = n % 1000000;
      int millarAlto = (int) (fragmento / 1000);
      int millarBajo = (int) (fragmento % 1000);
      n = n / 1000000;

      String nombreGrupo = _grupos[grupo];
      if (fragmento > 1 && grupo > 0) {
        nombreGrupo += "es";
      }

      if ((millarAlto != 0) || (millarBajo != 0)) {
        if (millarAlto > 1) {
          resultado = millarATexto(millarAlto) + " mil " + millarATexto(millarBajo) + " "
              + nombreGrupo + " " + resultado;
        }

        if (millarAlto == 0) {
          resultado = millarATexto(millarBajo) + " " + nombreGrupo + " " + resultado;
        }

        if (millarAlto == 1) {
          resultado = "mil " + millarATexto(millarBajo) + " " + nombreGrupo + " " + resultado;
        }
      }
      grupo++;
    }
    resultado = resultado.trim().concat(" ");
    return resultado;
  }

  @Override
  public String convert(BigDecimal number) {

    double num = number.doubleValue();

    num = (double) (Math.round(num * Math.pow(10, 2)) / Math.pow(10, 2));
    int number_whole = (int) num;
    int number_decimal = (int) ((num * 100) - (number_whole * 100));
    String value;
    if (number_decimal == 0) {
      value = numeroACastellano(number_whole).concat("00/100 M.N.");
    } else {
      value = numeroACastellano(number_whole).concat(Integer.toString(number_decimal))
          .concat("/100 M.N.");
    }
    value = value.substring(0, 1).toUpperCase().concat(value.substring(1));
    return value;
  }

}
